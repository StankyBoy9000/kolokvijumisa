package kolok.kolok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolok.kolok.dto.UcenikDTO;
import kolok.kolok.model.Ucenik;
import kolok.kolok.service.UcenikService;

@Controller
@RequestMapping(path = "/api/ucenici")
@CrossOrigin
public class UcenikController {
	@Autowired
	private UcenikService ucenikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<UcenikDTO>> getAllKorisnici(Pageable pageable) {
		return new ResponseEntity<Page<UcenikDTO>>(ucenikService.findAll(pageable).map(k->new UcenikDTO(k)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Ucenik> getUcenik(@PathVariable("id") Long id) {
		Ucenik ucenik = ucenikService.findOne(id);
		if (ucenik != null) {
			return new ResponseEntity<Ucenik>(ucenik, HttpStatus.OK);
		}
		return new ResponseEntity<Ucenik>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Ucenik> createUcenik(@RequestBody Ucenik novUcenik) {
		if (ucenikService.findOne(novUcenik.getId())!= null) {
			return new ResponseEntity<Ucenik>(HttpStatus.CONFLICT);
		}
		ucenikService.save(novUcenik);
		return new ResponseEntity<Ucenik>(novUcenik, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Ucenik> updateUcenik(@PathVariable("id") Long id,
			@RequestBody Ucenik novUcenik) {
		Ucenik ucenik = ucenikService.findOne(id);
		if (ucenik == null) {
			return new ResponseEntity<Ucenik>(HttpStatus.NOT_FOUND);
		}
		ucenik.setIme(novUcenik.getIme());
		ucenik.setPrezime(novUcenik.getPrezime());
		ucenikService.save(ucenik);
		return new ResponseEntity<Ucenik>(ucenik, HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteOdeljenje(@PathVariable("id") Long id) {
		if (ucenikService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		ucenikService.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
