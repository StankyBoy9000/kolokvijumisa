package kolok.kolok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import kolok.kolok.dto.OdeljenjeDTO;
import kolok.kolok.model.Odeljenje;
import kolok.kolok.service.OdeljenjeService;

@Controller
@RequestMapping(path = "/api/odeljenja")
@CrossOrigin
public class OdeljenjeController {
	@Autowired
	private OdeljenjeService odeljenjeService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<OdeljenjeDTO>> getAllOdeljenja(Pageable pageable) {
		return new ResponseEntity<Page<OdeljenjeDTO>>(odeljenjeService.findAll(pageable).map(k->new OdeljenjeDTO(k)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Odeljenje> getOdeljenje(@PathVariable("id") Long id) {
		Odeljenje odeljenje = odeljenjeService.findOne(id);
		if (odeljenje != null) {
			return new ResponseEntity<Odeljenje>(odeljenje, HttpStatus.OK);
		}
		return new ResponseEntity<Odeljenje>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Odeljenje> createOdeljenje(@RequestBody Odeljenje novoOdeljenje) {
		if (odeljenjeService.findOne(novoOdeljenje.getId())!= null) {
			return new ResponseEntity<Odeljenje>(HttpStatus.CONFLICT);
		}
		odeljenjeService.save(novoOdeljenje);
		return new ResponseEntity<Odeljenje>(novoOdeljenje, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Odeljenje> updateOdeljenje(@PathVariable("id") Long id,
			@RequestBody Odeljenje novoOdeljenje) {
		Odeljenje odeljenje = odeljenjeService.findOne(id);
		if (odeljenje == null) {
			return new ResponseEntity<Odeljenje>(HttpStatus.NOT_FOUND);
		}
		odeljenje.setNaziv(novoOdeljenje.getNaziv());
		odeljenje.setOgledno(novoOdeljenje.isOgledno());
		odeljenjeService.save(odeljenje);
		return new ResponseEntity<Odeljenje>(odeljenje, HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteOdeljenje(@PathVariable("id") Long id) {
		if (odeljenjeService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		odeljenjeService.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
