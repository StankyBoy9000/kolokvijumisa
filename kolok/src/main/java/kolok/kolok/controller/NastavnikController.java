package kolok.kolok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolok.kolok.dto.NastavnikDTO;
import kolok.kolok.model.Nastavnik;
import kolok.kolok.service.NastavnikService;

@Controller
@RequestMapping(path = "/api/nastavnici")
@CrossOrigin
public class NastavnikController {
	@Autowired
	private NastavnikService nastavnikService;
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<NastavnikDTO>> getAllNastavnici(Pageable pageable) {
		return new ResponseEntity<Page<NastavnikDTO>>(nastavnikService.findAll(pageable).map(k->new NastavnikDTO(k)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Nastavnik> getNastavnik(@PathVariable("id") Long id) {
		Nastavnik nastavnik = nastavnikService.findOne(id);
		if (nastavnik != null) {
			return new ResponseEntity<Nastavnik>(nastavnik, HttpStatus.OK);
		}
		return new ResponseEntity<Nastavnik>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Nastavnik> createNastavnik(@RequestBody Nastavnik noviNastavnik) {
		if (nastavnikService.findOne(noviNastavnik.getId())!= null) {
			return new ResponseEntity<Nastavnik>(HttpStatus.CONFLICT);
		}
		nastavnikService.save(noviNastavnik);
		return new ResponseEntity<Nastavnik>(noviNastavnik, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Nastavnik> updateNastavnik(@PathVariable("id") Long id,
			@RequestBody Nastavnik noviNastavnik) {
		Nastavnik nastavnik = nastavnikService.findOne(id);
		if (nastavnik == null) {
			return new ResponseEntity<Nastavnik>(HttpStatus.NOT_FOUND);
		}
		nastavnik.setIme(noviNastavnik.getIme());
		nastavnik.setPrezime(noviNastavnik.getPrezime());
		nastavnikService.save(nastavnik);
		return new ResponseEntity<Nastavnik>(nastavnik, HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteNastavnik(@PathVariable("id") Long id) {
		if (nastavnikService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		nastavnikService.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
