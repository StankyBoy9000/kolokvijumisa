package kolok.kolok.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kolok.kolok.dto.RoditeljDTO;
import kolok.kolok.model.Roditelj;
import kolok.kolok.service.RoditeljService;

@Controller
@RequestMapping(path = "/api/roditelji")
@CrossOrigin
public class RoditeljController {
	@Autowired
	private RoditeljService roditeljService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<RoditeljDTO>> getAllKorisnici(Pageable pageable) {
		return new ResponseEntity<Page<RoditeljDTO>>(roditeljService.findAll(pageable).map(k->new RoditeljDTO(k)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Roditelj> getRoditelj(@PathVariable("id") Long id) {
		Roditelj roditelj = roditeljService.findOne(id);
		if (roditelj != null) {
			return new ResponseEntity<Roditelj>(roditelj, HttpStatus.OK);
		}
		return new ResponseEntity<Roditelj>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Roditelj> createRoditelj(@RequestBody Roditelj novRoditelj) {
		if (roditeljService.findOne(novRoditelj.getId())!= null) {
			return new ResponseEntity<Roditelj>(HttpStatus.CONFLICT);
		}
		roditeljService.save(novRoditelj);
		return new ResponseEntity<Roditelj>(novRoditelj, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Roditelj> updateRoditelj(@PathVariable("id") Long id,
			@RequestBody Roditelj novRoditelj) {
		Roditelj roditelj = roditeljService.findOne(id);
		if (roditelj == null) {
			return new ResponseEntity<Roditelj>(HttpStatus.NOT_FOUND);
		}
		roditelj.setIme(novRoditelj.getIme());
		roditelj.setPrezime(novRoditelj.getPrezime());
		roditeljService.save(roditelj);
		return new ResponseEntity<Roditelj>(roditelj, HttpStatus.OK);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteOdeljenje(@PathVariable("id") Long id) {
		if (roditeljService.findOne(id) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		roditeljService.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
