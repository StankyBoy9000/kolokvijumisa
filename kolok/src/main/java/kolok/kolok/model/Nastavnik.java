package kolok.kolok.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity
@Table
@Inheritance(strategy = InheritanceType.JOINED)
public class Nastavnik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "razredniStaresina")
	private Odeljenje odeljenje;
	@Column(nullable = false, unique = false, length = 64)
	private String ime;
	@Column(nullable = false, unique = false, length = 64)
	private String prezime;
	
	public Nastavnik() {
		super();
	}
	
	public Nastavnik(Long id, Odeljenje odeljenje, String ime, String prezime) {
		super();
		this.id = id;
		this.odeljenje = odeljenje;
		this.ime = ime;
		this.prezime = prezime;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Odeljenje getOdeljenje() {
		return odeljenje;
	}
	public void setOdeljenje(Odeljenje odeljenje) {
		this.odeljenje = odeljenje;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nastavnik other = (Nastavnik) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
