package kolok.kolok.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Roditelj {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false, unique = false, length = 64)
	private String ime;
	@Column(nullable = false, unique = false, length = 64)
	private String prezime;
	@ManyToMany(mappedBy = "roditelji")
	private Set<Ucenik> ucenici = new HashSet<Ucenik>();
	
	public Roditelj() {
		super();
	}
	
	public Roditelj(Long id, String ime, String prezime, Set<Ucenik> ucenici) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.ucenici = ucenici;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Set<Ucenik> getUcenici() {
		return ucenici;
	}
	public void setUcenici(Set<Ucenik> ucenici) {
		this.ucenici = ucenici;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Roditelj other = (Roditelj) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
}
