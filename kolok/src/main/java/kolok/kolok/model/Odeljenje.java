package kolok.kolok.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Odeljenje {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false, unique = false, length = 64)
	private String naziv;
	@Column
	private boolean ogledno;
	@OneToOne(fetch = FetchType.LAZY)
	private Nastavnik razredniStaresina;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "odeljenje")
	private Set<Ucenik> ucenici = new HashSet<Ucenik>();
	
	public Odeljenje() {
		super();
	}
	
	public Odeljenje(Long id, String naziv, boolean ogledno, Nastavnik razredniStaresina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.ogledno = ogledno;
		this.razredniStaresina = razredniStaresina;
	}
	
	public Set<Ucenik> getUcenici() {
		return ucenici;
	}

	public void setUcenici(Set<Ucenik> ucenici) {
		this.ucenici = ucenici;
	}

	public Nastavnik getRazredniStaresina() {
		return razredniStaresina;
	}
	public void setRazredniStaresina(Nastavnik razredniStaresina) {
		this.razredniStaresina = razredniStaresina;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public boolean isOgledno() {
		return ogledno;
	}
	public void setOgledno(boolean ogledno) {
		this.ogledno = ogledno;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Odeljenje other = (Odeljenje) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
