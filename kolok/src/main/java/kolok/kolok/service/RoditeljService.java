package kolok.kolok.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import kolok.kolok.model.Roditelj;
import kolok.kolok.repository.RoditeljRepository;

@Service
public class RoditeljService {
	@Autowired
	RoditeljRepository roditeljRepository;

	public RoditeljService() {
		super();
	}
	
	public Page<Roditelj> findAll(Pageable pageable) {
		return roditeljRepository.findAll(pageable);
	}
	
	public Roditelj findOne(Long id) {
		return roditeljRepository.findById(id).orElse(null);
	}
	
	public void save(Roditelj roditelj) {
		roditeljRepository.save(roditelj);
	}
	
	public void delete(Long id) {
		roditeljRepository.deleteById(id);
	}
}
