package kolok.kolok.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import kolok.kolok.model.Nastavnik;
import kolok.kolok.repository.NastavnikRepository;

@Service
public class NastavnikService {
	@Autowired
	NastavnikRepository nastavnikRepository;

	public NastavnikService() {
		super();
	}
	
	public Page<Nastavnik> findAll(Pageable pageable) {
		return nastavnikRepository.findAll(pageable);
	}
	
	public Nastavnik findOne(Long id) {
		return nastavnikRepository.findById(id).orElse(null);
	}
	
	public void save(Nastavnik nastavnik) {
		nastavnikRepository.save(nastavnik);
	}
	
	public void delete(Long id) {
		nastavnikRepository.deleteById(id);
	}
}
