package kolok.kolok.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import kolok.kolok.model.Nastavnik;
import kolok.kolok.model.Ucenik;
import kolok.kolok.repository.UcenikRepository;

@Service
public class UcenikService {
	@Autowired
	UcenikRepository ucenikRepository;

	public UcenikService() {
		super();
	}
	
	public Iterable<Nastavnik> findRazredniStaresina(Long id) {
		return ucenikRepository.findNastavnikUsingUcenik(id);
	}
	
	public Page<Ucenik> findAll(Pageable pageable) {
		return ucenikRepository.findAll(pageable);
	}
	
	public Ucenik findOne(Long id) {
		return ucenikRepository.findById(id).orElse(null);
	}
	
	public void save(Ucenik ucenik) {
		ucenikRepository.save(ucenik);
	}
	
	public void delete(Long id) {
		ucenikRepository.deleteById(id);
	}
}
