package kolok.kolok.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import kolok.kolok.model.Odeljenje;
import kolok.kolok.model.Roditelj;
import kolok.kolok.repository.OdeljenjeRepository;

@Service
public class OdeljenjeService {
	@Autowired
	OdeljenjeRepository odeljenjeRepository;

	public OdeljenjeService() {
		super();
	}
	
	public Iterable<Roditelj> findGoodParents(){
		return odeljenjeRepository.findGoodRoditelji();
	}
	
	public Page<Odeljenje> findAll(Pageable pageable) {
		return odeljenjeRepository.findAll(pageable);
	}
	
	public Odeljenje findOne(Long id) {
		return odeljenjeRepository.findById(id).orElse(null);
	}
	
	public void save(Odeljenje odeljenje) {
		odeljenjeRepository.save(odeljenje);
	}
	
	public void delete(Long id) {
		odeljenjeRepository.deleteById(id);
	}
}
