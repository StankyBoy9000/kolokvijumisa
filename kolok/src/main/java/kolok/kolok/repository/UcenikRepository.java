package kolok.kolok.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kolok.kolok.model.Nastavnik;
import kolok.kolok.model.Ucenik;

@Repository
public interface UcenikRepository  extends PagingAndSortingRepository<Ucenik, Long>{

	@Query("select * from nastavnik where nastavnik.id = (select razredni_staresina_id from odeljenje where (select odeljenje_id from ucenik where ucenik.id = :ucenikId))")
	public Iterable<Nastavnik> findNastavnikUsingUcenik(@Param("ucenikId") Long ucenikId);
	
}
