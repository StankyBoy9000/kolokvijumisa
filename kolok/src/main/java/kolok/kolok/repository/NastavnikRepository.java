package kolok.kolok.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolok.kolok.model.Nastavnik;

@Repository
public interface NastavnikRepository  extends PagingAndSortingRepository<Nastavnik, Long>{

}
