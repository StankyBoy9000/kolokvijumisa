package kolok.kolok.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolok.kolok.model.Roditelj;

@Repository
public interface RoditeljRepository  extends PagingAndSortingRepository<Roditelj, Long>{

}
