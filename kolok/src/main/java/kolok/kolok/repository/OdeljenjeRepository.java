package kolok.kolok.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import kolok.kolok.model.Odeljenje;
import kolok.kolok.model.Roditelj;

@Repository
public interface OdeljenjeRepository  extends PagingAndSortingRepository<Odeljenje, Long>{

	
	
	@Query("select * from roditelj where id in (select roditelji_id from ucenik_roditelji where ucenici_id in (select id from ucenik where odeljenje_id in (select id from odeljenje where ogledno=True)))")
	public Iterable<Roditelj> findGoodRoditelji();
}
