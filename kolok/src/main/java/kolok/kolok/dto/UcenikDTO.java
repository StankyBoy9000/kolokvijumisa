package kolok.kolok.dto;

import java.util.HashSet;
import java.util.Set;

import kolok.kolok.model.Roditelj;
import kolok.kolok.model.Ucenik;

public class UcenikDTO {
	private Long id;
	private String ime;
	private String prezime;

	private Set<RoditeljDTO> roditelji = new HashSet<RoditeljDTO>();

	public UcenikDTO() {
		super();
	}

	public UcenikDTO(Long id, String ime, String prezime, Set<RoditeljDTO> roditelji) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.roditelji = roditelji;
	}
	
	public UcenikDTO(Ucenik ucenik) {
		super();
		this.id = ucenik.getId();
		this.ime = ucenik.getIme();
		this.prezime = ucenik.getPrezime();
		for (Roditelj r : ucenik.getRoditelji()) {
			roditelji.add(new RoditeljDTO(r.getId(), r.getIme(), r.getPrezime(), null));
		}
	}

	public UcenikDTO(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Set<RoditeljDTO> getRoditelji() {
		return roditelji;
	}

	public void setRoditelji(Set<RoditeljDTO> roditelji) {
		this.roditelji = roditelji;
	}
	
	
}
