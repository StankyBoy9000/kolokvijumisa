package kolok.kolok.dto;



import kolok.kolok.model.Nastavnik;
import kolok.kolok.model.Odeljenje;

public class OdeljenjeDTO {
	
	private Long id;
	private String naziv;
	private boolean ogledno;
	private NastavnikDTO razredniStaresina;
	
	public OdeljenjeDTO() {
		super();
	}
	
	public OdeljenjeDTO(Long id, String naziv, boolean ogledno, NastavnikDTO razredniStaresina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.ogledno = ogledno;
		this.razredniStaresina = razredniStaresina;
	}
	
	public OdeljenjeDTO(Odeljenje odeljenje) {
		super();
		this.id = odeljenje.getId();
		this.naziv = odeljenje.getNaziv();
		this.ogledno = odeljenje.isOgledno();
		this.razredniStaresina = new NastavnikDTO(odeljenje.getRazredniStaresina());
	}
	

	public OdeljenjeDTO(Long id, String naziv, boolean ogledno) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.ogledno = ogledno;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public boolean isOgledno() {
		return ogledno;
	}

	public void setOgledno(boolean ogledno) {
		this.ogledno = ogledno;
	}

	public NastavnikDTO getRazredniStaresina() {
		return razredniStaresina;
	}

	public void setRazredniStaresina(NastavnikDTO razredniStaresina) {
		this.razredniStaresina = razredniStaresina;
	}
	
	
	
	
}
