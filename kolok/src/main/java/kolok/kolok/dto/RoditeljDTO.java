package kolok.kolok.dto;



import java.util.HashSet;
import java.util.Set;

import kolok.kolok.model.Roditelj;
import kolok.kolok.model.Ucenik;

public class RoditeljDTO {
	private Long id;
	private String ime;
	private String prezime;
	
	private Set<UcenikDTO> ucenici = new HashSet<UcenikDTO>();

	public RoditeljDTO() {
		super();
	}
	
	public RoditeljDTO(Roditelj roditelj) {
		this.id = roditelj.getId();
		this.ime = roditelj.getIme();
		this.prezime = roditelj.getPrezime();
		for (Ucenik r : roditelj.getUcenici()) {
			ucenici.add(new UcenikDTO(r.getId(), r.getIme(), r.getPrezime(), null));
		}
	}

	public RoditeljDTO(Long id, String ime, String prezime, Set<UcenikDTO> ucenici) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.ucenici = ucenici;
	}

	public RoditeljDTO(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Set<UcenikDTO> getUcenici() {
		return ucenici;
	}

	public void setUcenici(Set<UcenikDTO> ucenici) {
		this.ucenici = ucenici;
	}
	
	
}
