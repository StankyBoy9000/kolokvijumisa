package kolok.kolok.dto;



import kolok.kolok.model.Nastavnik;
import kolok.kolok.model.Odeljenje;

public class NastavnikDTO {

	private Long id;
	private OdeljenjeDTO odeljenje;
	private String ime;
	private String prezime;
	
	public NastavnikDTO() {
		super();
	}
	
	public NastavnikDTO(Long id, OdeljenjeDTO odeljenje, String ime, String prezime) {
		super();
		this.id = id;
		this.odeljenje = odeljenje;
		this.ime = ime;
		this.prezime = prezime;
	}
	
	public NastavnikDTO(Nastavnik nastavnik) {
		super();
		this.id = nastavnik.getId();
		this.odeljenje = new OdeljenjeDTO(nastavnik.getOdeljenje());
		this.ime = nastavnik.getIme();
		this.prezime = nastavnik.getPrezime();
	}
	

	public NastavnikDTO(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OdeljenjeDTO getOdeljenje() {
		return odeljenje;
	}

	public void OdeljenjeDTO(OdeljenjeDTO odeljenje) {
		this.odeljenje = odeljenje;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	
	
}
